<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <title>Título</title>
    <script src="../../dist/1.0.0/pecas-reader.min.js"></script>
  </head>
  <body>
    <h1 id="quien-respalda-a-quien">¿Quién respalda a quién?</h1>
    <p>Entre editores y lectores es común escuchar sobre las «copias digitales». Esto implica que los libros electrónicos tienden a verse como respaldos de los libros impresos. La manera en como el primero se convierte en una copia del original —aunque primero necesites un archivo digital para poder imprimir— es algo similar a lo siguiente:</p>
    <ol>
      <li>
        <p>Los archivos digitales (\textsc{ad}) con un apropiado mantenimiento pueden tener mayores probabilidades de durar más que su correlato material.</p>
      </li>
    </ol>
    <ol>
      <li>
        <p>Los archivos físicos (\textsc{af}) están constreñidos por cuestiones geopolíticas, como las modificaciones en las políticas culturales, o por eventos aleatorios, como los cambios en el medio ambiente o los accidentes.</p>
      </li>
    </ol>
    <ol>
      <li>
        <p><em>Por lo tanto</em>, los \textsc{ad} son el respaldo de los \textsc{af} porque <em>en teoría</em> su dependencia solo es técnica.</p>
      </li>
    </ol>
    <p>La famosa copia digital emerge como un derecho a la copia privada. ¿Qué tal si un día nuestros impresos son censurados o quemados? O quizá una lluvia o un derrame de café puede chingar nuestra colección de libros. Quién sabe, los \textsc{ad} parecen ser más confiables.</p>
    <p>Pero hay un par de suposiciones en este argumento. (1) La tecnología detrás de los \textsc{ad} de una manera u otra siempre hará que los datos fluyan. Tal vez esto se debe a que (2) una característica —parte de su «naturaleza»— de la información es que nadie puede detener su propagación. Esto puede implicar que (3) los <em>hackers</em> siempre pueden destruir cualquier tipo de sistema de gestión de derechos digitales.</p>
    <p>Sin duda algunas personas van a poder <em>hackear</em> las cerraduras pero a un costo muy alto: cada vez que un algoritmo criptográfico es revelado, otro más complejo ya viene en camino —<em>Barlow dixit</em>—. No podemos confiar en la idea de que nuestra infraestructura digital será diseñada para compartir libremente… Además, ¿cómo se puede probar que la información quiere ser libre sin recaer en su «naturaleza» o tornarla en alguna clase de sujeto autónomo?</p>
    <p>Por otra parte, la dinámica entre las copias y los originales genera un orden jerárquico. Cada \textsc{ad} se encuentra en una posición secundaria porque es una copia. En un mundo lleno de cosas, la materialidad es una característica relevante para los bienes comunes y las mercancías; muchas personas van a preferir los \textsc{af} ya que, bueno, puedes asirlos.</p>
    <p>El mercado de los libros electrónicos muestra que esta jerarquía al menos se está matizando. Para ciertos lectores los \textsc{ad} ahora están en la cúspide de la pirámide. Se puede señalar este fenómeno con el siguiente argumento:</p>
    <ol>
      <li>
        <p>Los \textsc{ad} son mucho más flexibles y sencillos de compartir.</p>
      </li>
    </ol>
    <ol>
      <li>
        <p>Los \textsc{af} son muy rígidos y no hay facilidad para su acceso.</p>
      </li>
    </ol>
    <ol>
      <li>
        <p><em>Por lo tanto</em>, los \textsc{ad} son más convenientes que los \textsc{af}.</p>
      </li>
    </ol>
    <p>De repente los \textsc{af} mutan en las copias duras donde los datos serán guardados tal cual fueron publicados. Si se requiere, su información está a disposición para la extracción y el procesamiento.</p>
    <figure>
      <img src="../img/p004_i001.jpg" alt="Programando Libreros mientras escanea libros cuyos \textsc{ad} no son aptos para la \textsc{p\&#38;r} o simplemente no existen; ¿ves cómo no es necesario tener un pinche escáner bueno?"/>
      <figcaption>
        Programando Libreros mientras escanea libros cuyos \textsc{ad} no son aptos para la \textsc{p\&#38;r} o simplemente no existen; ¿ves cómo no es necesario tener un pinche escáner bueno?
      </figcaption>
    </figure>
    <p>Sí, aquí también tenemos un par de suposiciones. De nuevo (1) confiamos en la estabilidad de nuestra infraestructura digital que nos permitirá tener acceso a nuestros \textsc{ad} sin importar que tan viejos estén. (2) La prioridad de los lectores es sobre el uso de los archivos —si no su mero consumo— y no su preservación y reproducción (\textsc{p\&#38;r}). (3) El argumento asume que los respaldos son información inmóvil, donde los estantes son refrigeradores para los libros que después se usarán.</p>
    <p>El optimismo respecto a nuestra infraestructura digital es muy alto. Por lo general la vemos como una tecnología que nos da acceso a chorrocientos archivos y no como una maquinaria para la \textsc{p\&#38;r}. Esto puede ser problemático porque en ciertos casos los formatos de archivos que fueron diseñados para su uso común no son los más adecuados para su \textsc{p\&#38;r}. Como ejemplo tenemos el uso de \textsc{pdf} a modo de libros electrónicos. Si le damos mucha importancia a la prioridad de los lectores, como consecuencia podemos llegar a una situación donde la única manera de procesar los datos es el retorno a su extracción a partir de las copias duras. Cuando lo llevamos a cabo de esa manera tenemos otro dolor de cabeza: las correcciones del contenido tienen que ser añadidas a la última edición disponible en copia dura. Pero ¿puedes adivinar dónde están todas estas correcciones? Lo más seguro es que no. A lo mejor deberíamos por empezar a pensar los respaldos como algún tipo de <em>actualización continua</em>.</p>
    <p>Tal como imaginas —y comenzamos a vivir en— escenarios con un alto control en la transferencia de datos, podemos fantasear con una situación donde por alguna razón nuestras fuentes de energía eléctrica no están disponibles o tienen poco abastecimiento. En este contexto todas las fortalezas de los \textsc{ad} pierden sentido. A lo mejor no serán accesibles. Quizá no podrán propagarse. Por el momento es difícil de concebir. Generación tras generación los \textsc{ad} guardados en discos duros mutarán en una herencia que hace patente la esperanza de volver a usarlos de nuevo. Pero con el tiempo estos dispositivos, que contienen nuestro patrimonio cultural, se convertirán en objetos extraños sin utilidad aparente.</p>
    <p>Las características de los \textsc{ad} que nos hacen ver la fragilidad de los \textsc{af} van a desaparecer en su ocultamiento. ¿Aún podemos hablar de información si esta es potencial —sabemos que los datos están ahí, pero son inaccesibles ya que no tenemos los medios para verlos—? ¿O acaso la información ya implica los recursos técnicos para su acceso —es decir, no existe la información sin un sujeto con las capacidades técnicas para extraer, procesar y usar los datos—?</p>
    <p>Cuando por lo común hablamos sobre la información, ya suponemos que está ahí pero en varias ocasiones no es accesible. Así que la idea de información potencial podría ser contraintuitiva. Si la información no está en acto, de manera llana consideramos que es inexistente, no que se encuentra en algún estado potencial.</p>
    <p>A la par que nuestra tecnología está en desarrollo, nosotros asumimos que siempre habrá <em>la posibilidad</em> de dar con mejores maneras para extraer e interpretar los datos; y, por ello, que existen más oportunidades de cosechar nuevos tipos de información —y de obtener ganancias con ello—. La preservación de los datos yace entre estas posibilidades debido a que casi siempre respaldamos archivos con la idea de que los podríamos necesitar de nuevo.</p>
    <figure>
      <img src="../img/p004_i002.jpg" alt="Programando Libreros y Hacklib mientras trabajan en un proyecto cuyo objetivo es la \textsc{p\&#38;r} de libros viejos de ciencia ficción latinoamericana; en ciertas ocasiones un escáner en forma de V es necesario cuando los libros son muy frágiles."/>
      <figcaption>
        Programando Libreros y Hacklib mientras trabajan en un proyecto cuyo objetivo es la \textsc{p\&#38;r} de libros viejos de ciencia ficción latinoamericana; en ciertas ocasiones un escáner en forma de V es necesario cuando los libros son muy frágiles.
      </figcaption>
    </figure>
    <p>Nuestro mundo se vuelve más complejo por las nuevas cosas que están a nuestra disposición, en muchos casos como nuevas características de cosas que ya conocemos. Las políticas de preservación implican un optimismo epistémico y no solo un anhelo de mantener nuestro patrimonio vivo o incorrupto. No respaldaríamos datos si antes no creyéramos que podríamos necesitarlos en un futuro donde aún podemos utilizarlos.</p>
    <p>Con este ejercicio se puede ver con claridad una posible paradoja de los \textsc{ad}. Para tener más acceso se tiende a requerir una mayor infraestructura técnica. Esto puede implicar una mayor dependencia tecnológica que subordina la accesibilidad de la información a la disposición de los medios técnicos. <em>Por lo tanto</em>, nos encontramos con una situación donde una mayor accesibilidad es proporcional a una mayor infraestructura tecnológica y —tal como lo vemos en nuestros días— dependencia.</p>
    <p>El acceso abierto al conocimiento supone al menos unos requerimientos técnicos mínimos. Sin ello no podemos en realidad hablar de accesibilidad de la información. Las posibilidades del acceso abierto contemporáneo están restringidas a una dependencia tecnológica ya existente porque le prestamos más atención a la flexibilidad que los \textsc{ad} nos ofrecen para <em>su uso</em>. En un mundo sin fuentes de energía eléctrica este tipo de acceso se vuelve estrecho y un esfuerzo inútil.</p>
    <p>Así que, <em>¿quién respalda a quién?</em> En nuestro mundo, donde la geopolítica y los medios técnicos restringen el flujo de los datos y las personas al mismo tiempo que defiende al acceso a internet como un derecho humano —un tipo de discurso neoilustrado—, los \textsc{ad} son el salvavidas en una situación donde no tenemos otras formas de movernos alrededor o de escapar —no solo de frontera en frontera, sino también en el ciberespacio: se está volviendo un lugar común la necesidad de inscripción y de cesión de tu identidad con el fin de usar servicios <em>web</em>—. Vale la pena recordar que el acceso abierto a los datos puede ser un camino para mejorar como comunidad pero también podría constituirse en un método para perpetuar las condiciones sociales.</p>
    <p>No muchas personas tienen el privilegio que gozamos cuando hablamos sobre el acceso a los medios técnicos. Incluso de manera más desconcertante hay compas con incapacidades que les complican el acceso a la información aunque cuenten con los medios. ¿Acaso no es gracioso que nuestras ideas vertidas en un archivo puedan moverse de manera más «libre» que nosotros —tus memes pueden llegar a plataformas <em>web</em> donde tu presencia no está autorizada—?</p>
    <p>Deseo más desarrollos tecnológicos en pos de la libertad de la \textsc{p\&#38;r} y no solo de su uso como goce —no importa si es con fines intelectuales o de consumo\mbox{—.} Quiero que seamos libres. Pero en algunos casos las libertades sobre el uso de datos, la \textsc{p\&#38;r} de la información y la movilidad de las personas no se llevan bien.</p>
    <p>Con los \textsc{ad} obtenemos una mayor independencia en el uso de los archivos porque una vez que han sido guardados, pueden propagarse. No importan las barreras políticas o religiosas; la batalla toma lugar principalmente en el campo técnico. Pero esto no le da a los \textsc{ad} una mayor autonomía en su \textsc{p\&#38;r}. Tampoco implica que podamos obtener libertades personales o comunitarias. Los \textsc{ad} son objetos. <em>Los \textsc{ad} son herramientas</em> y cualquiera que los use mejor, cualquiera que sea su dueño, tendrá más poder.</p>
    <p>Con los \textsc{af} cabe la oportunidad de que tengamos más libertad para su \textsc{p\&#38;r}. Podemos hacer cualquier cosa que queramos con ellos: extraer sus datos, procesarlos y liberarlos. Pero solo si somos sus propietarios. En muchos casos no es el caso, así que los \textsc{af} tienden a tener un acceso más restringido para su uso. Y, de nueva cuenta, esto no implica que podamos ser libres. No existe una relación causa y efecto entre lo que un objeto hace posible y la manera en como un sujeto quiere ser libre. Los \textsc{af} son herramientas, no son amos ni esclavos, solo un medio para cualquiera que los use… pero ¿con qué fines?</p>
    <figure>
      <img src="../img/p004_i003.jpg" alt="Debido a la restricción en el acceso a los \textsc{af}, a veces es necesario un escáner portable en forma de V; este modelo nos permite manejar libros deteriorados así como podemos guardarlo en una mochila."/>
      <figcaption>
        Debido a la restricción en el acceso a los \textsc{af}, a veces es necesario un escáner portable en forma de V; este modelo nos permite manejar libros deteriorados así como podemos guardarlo en una mochila.
      </figcaption>
    </figure>
    <p>Necesitamos los \textsc{ad} y a los \textsc{af} como respaldos y como objetos de uso diario. El acto de respaldar es una categoría dinámica. Los archivos respaldados no son inertes ni son sustratos que esperan ser usados. En algunos casos vamos a usar los \textsc{af} porque los \textsc{ad} han sido corrompidos o su infraestructura tecnológica ha sido suspendida. En otras ocasiones vamos a usar los \textsc{ad} cuando los \textsc{af} han sido destruidos o restringidos.</p>
    <p>Así que la lucha en relación con los respaldos —y a toda esa mierda acerca de la «libertad» en las comunidades del <em>software</em> libre y del código abierto— no es solo en torno al reino «incorpóreo» de la información. Tampoco sobre los medios técnicos que posibilitan los datos digitales. Ni mucho menos respecto a las leyes que transforman la producción en propiedad. Tenemos otros frentes de batalla en contra del monopolio del ciberespacio —o como Lingel dice: la gentrificación del internet.</p>
    <p>No es solo acera del <em>software</em>, del <em>hardware</em>, de la privacidad, de la información o de las leyes. Se trata de nosotros: sobre cómo construimos comunidades y cómo la tecnología nos constituye como sujetos. <em>Necesitamos más teoría</em>. Pero una diversificada porque estar en internet no es lo mismo para un académico, un editor, una mujer, un niño, un refugiado, una persona no-blanca, un pobre o una anciana. Este espacio no es neutral ni homogéneo ni bidimensional. Se compone de cables, posee servidores, implica la explotación laboral, se conserva en edificios, <em>tiene poder</em> y, bueno, goza de todas las cosas del «mundo real». Que uses un dispositivo para su acceso no significa que en cualquier momento puedes decidir si estás conectado o no: siempre estás en línea sea como usuario, como consumidor o como dato.</p>
    <p><em>¿Quién respalda a quién?</em> Así como el internet nos está cambiando tal cual lo hizo la imprenta, lo archivos respaldados no son datos guardados sino <em>la memoria de nuestro mundo</em>. ¿Aún es buena idea dejar el trabajo de su \textsc{p\&#38;r} a un par de compañías de <em>hardware</em> y de <em>software</em>? ¿Podemos ya decir que el acto de respaldar implica archivos pero también algo más?</p>
  </body>
</html>