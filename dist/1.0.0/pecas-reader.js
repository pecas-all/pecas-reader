class PecasReader {
  constructor () {
    /*
    1. Buscar sections
    2. Buscar articles
    3. Comprar ambos conjuntos y resolver si una contiene a la otra:
      v. Se queda el conjunto con la raíz, se descarta el otro
      f. Los contierte todos a section
    4. Se coloca un header, nav, aside y footer vacíos
      (cada tema de Pecas Reader decidirá cómo poblar estas etiquetas a partir de una consulta a los metadatos en el header)
    */
    function getBodyChildrens () {
      let section = document.createElement('section');
      while (document.getElementsByTagName('div').length > 0) {
        unwrapTag('div');
      }
      return [section.appendChild(document.body.children)];
    }

    function getDOM (e, strict = false) {
      let dom = document.querySelectorAll(e);
      return strict ? dom.length > 0 ? dom : false : dom;
    }

    function getMain (a, b) {
      let parent = hasChilds(a, b) ? a : hasChilds(b, a) ? b : false,
        siblings = parent ? parent : getDom('section article', true),
        main = siblings ? Array.from(siblings) : getBodyChildrens(),
        clones = [];
      for (e of main) {
        clones.push(e.cloneNode(true));
      }
      return clones;
    }

    function hasChilds (a, b) {
      let childs = false;
      if (a.length > 0) {
        for (e of a) {
          if (b[0] && e.querySelectorAll(b[0].tagName).length > 0) {
            childs = true;
            break;
          }
        }
      }
      return childs;
    }

    function unwrapTag (name) {
      let els = document.getElementsByTagName(name);
      for (e of els) {
        let root = e.parentNode;
        while(e.firstChild) root.insertBefore(e.firstChild, e);
        root.removeChild(e);
      }
    }

    let sections = getDOM('section'),
      articles = getDOM('article'),
      main = getMain(sections, articles);
  }
}
